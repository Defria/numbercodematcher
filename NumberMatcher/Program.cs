﻿using System;
using System.Linq;

namespace NumberMatcher
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var generator = new NumberCodes(@"/Users/defria/RiderProjects/NumberMatcher/NumberMatcher/Codes.txt");
            var filter = generator.Codes.Where(x => x.Key == 7);
            foreach (var code in filter)
            {
                Console.WriteLine(code.Value);
            }
        }
    }
}