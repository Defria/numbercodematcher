using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NumberMatcher
{
    public class NumberCodes
    {
        public NumberCodes(string path)
        {
            Codes = new Dictionary<int, string>();
            GetCodes(path);
        }

        public Dictionary<int, string> Codes { get; }

        public void GetCodes(string path)
        {
            using (var sr = new StreamReader(path))
            {
                while (sr.Peek() >= 0)
                {
                    var line = sr.ReadLine();
                    if (line == null) continue;
                    var key = int.Parse(line.Split('=')[0]);
                    var value = line.Split('=')[1];
                    if (Codes.ContainsKey(key))
                    {
                        UpdateKey(key, value);
                    }
                    else
                    {
                        Codes.Add(key, value);
                    }
                }
            }
        }

        private void UpdateKey(int key, string value)
        {
            var currentValue = Codes.FirstOrDefault(k => k.Key == key).Value;
            Codes.Remove(key);
            Codes.Add(key, currentValue + $",{value}");
        }
    }
}